"use strict";

var fs = require('fs');

try {
  var config_data = fs.readFileSync('config.json', 'utf8');
  var config = JSON.parse(config_data);
  module.exports = config;
}
catch (err) {
  console.error("Failed to load config.json file");
  console.error(err);
  process.exit(2);
}
