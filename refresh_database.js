"use strict";

var mysql  = require('mysql2');
var fs     = require('fs');
var config = require('./config');

var db = mysql.createConnection({
  host      : config.db.host,
  user      : config.db.user,
  password  : config.db.password,
  database  : config.db.database
});

db.connect((err) => {
  if (err) {
    console.error("Failed to connect to database");
    console.error(err);
    process.exit(2);
  }
  else {
    console.log("Successfully connected to database as id: " + db.threadId);
  }
});

function printResult(task, fatal){
	return (err, results) => {
		if(err){
			if(fatal){
				console.error(task + " FAILED");
				console.error(err);
				process.exit(1);
			} else {
				console.log("Warn: Failed non-vital task: " + task);
			}
		} else {
			console.log(task + " okay");
		}
	};
}


console.log("Loading database schema");
var schema = fs.readFileSync(__dirname + '/schema.sql', 'utf8');

console.log("Recreating database...");
db.query('DROP DATABASE ' + config.db.database, printResult("Dropping old database", false));
db.query('CREATE DATABASE ' + config.db.database, printResult("Creating database", true));
db.query('USE '             + config.db.database, printResult("Using new database", true));
db.query(schema, (err, results) => {
	printResult('Executing schema file', true)(err, results);
  db.destroy();
});
