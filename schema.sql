CREATE TABLE Users(
  id        int          NOT NULL UNIQUE AUTO_INCREMENT,
  user      varchar(255) NOT NULL UNIQUE,
  email     varchar(255) NOT NULL UNIQUE,
  score     int          NOT NULL DEFAULT 0,
  password  varchar(255) NOT NULL,
  priv      int          NOT NULL DEFAULT 0,
  active    int          NOT NULL DEFAULT 0,
  PRIMARY KEY(id)
);
