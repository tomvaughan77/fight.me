"use strict";

var express     = require('express');
var http        = require('http');
var https       = require('https');
var fs          = require('fs');
var bodyParser  = require('body-parser');
var mysql       = require('mysql2');
var config      = require('./config');
var bcrypt      = require('bcrypt');
var session     = require('client-sessions');
var engine      = require('ejs-locals');
var isemail     = require('isemail');
var nodemailer  = require('nodemailer');
var sha1        = require('sha1');
var querystring = require('querystring');

var app         = express();

var privateKey  = fs.readFileSync('encryption/server.key', 'utf8');
var certificate = fs.readFileSync('encryption/server.crt', 'utf8');

var credentials = {
  key: privateKey,
  cert: certificate
};

var httpsServer = https.createServer(credentials, app);

var io          = require('socket.io')(httpsServer);

const saltRounds = config.bcrypt.rounds;

var currentUsers = [];
var currentSubjects = [];
var totalUsers = 0;
var suggestedSubjects = [];

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('data/subjects.txt')
});


lineReader.on('line', function (line) {
  suggestedSubjects.push(line);
});

var transporter = nodemailer.createTransport({
  host: "smtp-mail.outlook.com",
  secureConnection: false,
  port: 587,
  tls: {
    ciphers: 'SSLv3'
  },
  auth: {
    user: config.email.user,
    pass: config.email.password
  }
});

var db = mysql.createConnection({
  host      : config.db.host,
  user      : config.db.user,
  password  : config.db.password,
  database  : config.db.database,
  multipleStatements: true
});

db.connect(function(err) {
  if (err) throw err;
  console.log("Database connected!");
});

function lowestFreeIndex() {
  return new Promise((resolve, refuse) => {
    if (currentUsers.length === 0) {
      resolve(0);
    }
    else {
      let tick = 0;
      let found = false;
      for (var i=0; i<currentUsers.length; i++) {
        if (tick === currentUsers[i].id) {
          tick++;
        }
        else {
          resolve(tick);
          found = true;
          break;
        }
      }
      if (!found) {
        resolve(currentUsers.length);
      }
    }
  })
}

//Find subject in currentSubjects list an incrementSubject how many people are playing it
function updateSubject(subject, isInc) {
  var found = false;
  for (var i=0; i<currentSubjects.length; i++) {
    if (currentSubjects[i].name === subject) {
      found = true;
      if (isInc) {
        currentSubjects[i].players++;
      }
      else {
        currentSubjects[i].players--;
      }
    }
  }
  if (!found && isInc) {
    currentSubjects.push({
      name : subject,
      players : 1
    });
  }
}

function addUser(user, email, password, password_check) {
  return new Promise((resolve, reject) => {
    var priv = 0;
    var active = 0;
    db.query("SELECT * FROM Users", (err, rows) => {
      if (err) {
        reject(err);
        return;
      }
      else if (rows.length === 0) {
        priv = 2;
        active = 1;
      }
      else {
        for (let i=0; i<rows.length; i++) {
          if (user === rows[i].user) {
            resolve("User already exists");
            return;
          }
          if (email === rows[i].email) {
            resolve("Email already exists");
            return;
          }
        }
      }
      if (user.length < 1 || user.length > 254) {
        resolve("User length invalid");
        return;
      }
      if (email.length < 1 || email.length > 254) {
        resolve("Email length invalid");
        return;
      }
      if (!isemail.validate(email)) {
        resolve("Invalid email address");
        return;
      }
      if (password.length < 4) {
        resolve("Password too short. Seriously friendo, use a better password");
        return;
      }
      if (password !== password_check) {
        resolve("Passwords do not match");
        return;
      }
      bcrypt.hash(password, config.bcrypt.rounds, function(err, hash) {
        db.query("INSERT INTO Users (user, password, email, priv, active) VALUES (?, ?, ?, ?, ?)", [user, hash, email, priv, active], (err, rows) => {
          if (err) {
            reject(err);
          }
          else {
            resolve(true);
          }
        });
      });
    });
  })
}

function validateUser(id, user, email, password, password_check) {
  return new Promise((resolve, reject) => {
    db.query("SELECT * FROM Users WHERE NOT id=?", [id], (err, rows) => {
      if (err) {
        reject(err);
        return;
      }
      else {
        for (let i=0; i<rows.length; i++) {
          if (user === rows[i].user) {
            resolve("User already exists");
            return;
          }
          if (email === rows[i].email) {
            resolve("Email already exists");
            return;
          }
        }
        if (user.length < 1 || user.length > 254) {
          resolve("User length invalid");
          return;
        }
        if (email.length < 1 || email.length > 254) {
          resolve("Email length invalid");
          return;
        }
        if (!isemail.validate(email)) {
          resolve("Invalid email address");
          return;
        }
        if (password.length < 4) {
          resolve("Password too short. Seriously friendo, use a better password");
          return;
        }
        if (password !== password_check) {
          resolve("Passwords do not match");
          return;
        }
      }
      resolve(true);
    });
  })
}

function authenticateLogin(user, password) {
  return new Promise((resolve) => {
    db.query("SELECT * FROM Users WHERE user=?", [user], (err, rows) => {
      if (err) {
        throw err;
      }
      else if (rows.length > 0) {
        if (rows[0].active === 1) {
          bcrypt.compare(password, rows[0].password, (err, res) => {
            if (res === true) {
              resolve(true);
            }
            else {
              resolve("Incorrect password");
            }
          });
        }
        else {
          resolve("User not activated");
        }
      }
      else {
        resolve("User does not exist");
      }
    });
  })
}

function sendActivationEmail(req) {
  var mailOptions = {
    from: config.email.user,
    to: req.body.email,
    subject: "Fight.me Account Activation",
    text: "Follow this link to activate your Fight.me account: " + config.site.domain + ':' + config.site.port + '/activate?checksum=' + sha1(req.body.user)
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Activation email sent: ' + info.response);
    }
  });
}

function sendResetEmail(email) {
  var mailOptions = {
    from: config.email.user,
    to: email,
    subject: "Fight.me Password Reset",
    text: "Follow this link to reset the password of your Fight.me account: " + config.site.domain + ':' + config.site.port + '/reset?checksum=' + sha1(email) + '. Once you follow this link, your new password will be: ' + sha1(email) + '. You can change this back to something more human friendly under your account settings'
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Reset email sent: ' + info.response);
    }
  });
}

app.engine('ejs', engine);
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use('/', express.static(__dirname + '/views'));

app.use(session({
  cookieName: 'session',
  secret: sha1(config.session.secret),
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  cookie: {
    ephemeral: false
  }
}));

app.get('/', (req, res) => {
  io.on('connection', function(socket){
    function sendTable() {
      socket.emit('table update', new Date().toUTCString(), totalUsers, currentSubjects);
    }
    socket.emit('suggested update', suggestedSubjects);
    sendTable();
    setInterval(sendTable, 1000);
  });
  var msg = "";
  if (req.query.message) {
    res.render('index.ejs', {
      session: req.session,
      join : "false",
      isNew : "",
      subject : "",
      message : req.query.message
    });
  }
  else {
    res.render('index.ejs', {
      session: req.session,
      join : "false",
      isNew : "",
      subject : "",
      message : ""
    });
  }
});

app.post('/join-chatroom', (req, res) => {
  //res.render('index.ejs', {session: req.session});
  if (req.body.isNew === "true") {
    res.render('index.ejs', {
      session: req.session,
      join : "true",
      isNew : req.body.isNew,
      subject : "",
      message : ""
    });
  }
  else {
    res.render('index.ejs', {
      session: req.session,
      join : "true",
      isNew : req.body.isNew,
      subject : req.body.subject,
      message : ""
    });
  }
});

app.post('/chatroom', function(req, res){
  lowestFreeIndex().then(function(result) {
    currentUsers.push({
      id : result,
      name : req.body.alias,
      subject : req.body.subject
    });
    updateSubject(req.body.subject, true);
    totalUsers++;
    console.log("lowestFreeIndex: " + result);
    return result;
  }).then(function(myid) {
    var this_user = currentUsers.find(x => x.id === myid);
    console.log("=====================");
    console.log("Chat ID  : " + myid);
    console.log("User     : " + req.session.user);
    console.log("Alias    : " + this_user.name);
    console.log("Subject  : " + this_user.subject);
    console.log("=====================");
    console.log(currentUsers);
    console.log("=====================");

    res.render('chatroom.ejs');

    io.once('connection', function(socket){
      socket.join(this_user.subject);

      socket.emit('start message', 'Welcome ' + this_user.name + '. You are chatting with:', myid, this_user.subject);
      for (let i=0; i<currentUsers.length; i++) {
        if (currentUsers[i].subject === this_user.subject && this_user.id !== currentUsers[i].id) {
          socket.emit('start message', currentUsers[i].name);
        }
      }

      socket.to(this_user.subject).emit('join message', this_user.name + ' has joined.');

      console.log('User ' + this_user.name + ' connected');

      socket.on('chat message', function(msg, id){
        var user = currentUsers.find(x => x.id === id);
        console.log('ID: ' + id + ' Message: ' + msg);
        socket.to(user.subject).emit('chat message', user.name + ': ' + msg);
        socket./*to(currentUsers[id].subject).*/emit('chat message', user.name + ': ' + msg);
      });

      socket.on('disconnect', function() {
        console.log("Disconnect");
        console.log(currentUsers);
        socket.to(this_user.subject).emit('disconnect message', this_user.name + ' has disconnected');
        totalUsers--;
        updateSubject(this_user.subject, false);
        currentUsers.splice(myid, 1);
        console.log(currentUsers);
      });
    });
  });
});

app.post('/login', function(req, res){
  authenticateLogin(req.body.username, req.body.password).then(function (result) {
    if (result === true) {
      console.log("Auth succeeded");
      req.session.user = req.body.username;
      let query = querystring.stringify({
        "message": "Welcome, " + req.body.username
      });
      res.redirect('/?' + query);
    }
    else {
      console.log("Auth failed");
      let query = querystring.stringify({
        "message": "Authentication failed: " + result
      });
      res.redirect('/?' + query);
    }
  });
});

app.get('/logout', (req, res) => {
  req.session.destroy();
  res.redirect('/');
});

app.get('/about', (req, res) => {
  res.render('about.ejs');
});

app.post('/signup', (req, res) => {
  addUser(req.body.user, req.body.email, req.body.password, req.body.passwordConfirm)
  .catch(err => {
    console.log("Adding user failed");
    throw err;
  })
  .then(result => {
    if (result === true) {
      console.log("User " + req.body.user + " successfully added to database");
      var query = querystring.stringify({
        "message": "User " + req.body.user + " successfully added to database. Please check your linked email account for the link to active your account"
      });
      sendActivationEmail(req);
      res.redirect('/?' + query);
    }
    else {
      console.log("User " + req.body.user + " validation failed");
      res.render('signup.ejs', {message : result});
    }
  });
});

app.get('/signup', (req, res) => {
  res.render('signup.ejs', {passed : "false", message : ""});
});

app.get('/activate', (req, res) => {
  console.log("Activating . . .");
  db.query("SELECT * FROM Users", (err, rows) => {
    if (err) {
      throw err;
    }
    for (let i=0; i<rows.length; i++) {
      if (sha1(rows[i].user) === req.query.checksum) {
        db.query("UPDATE Users SET active=1 WHERE user=?", [rows[i].user], (err, out) => {
          if (err) {
            throw err;
          }
          else {
            console.log("User " + rows[i].user + " successfully activated");
          }
        });
      }
    }
  });
  res.redirect('/');
});

app.get('/reset', (req, res) => {
  db.query("SELECT * FROM Users", (err, rows) => {
    if (err) {
      throw err;
    }
    else {
      for (let i=0; i<rows.length; i++) {
        if (sha1(rows[i].email) === req.query.checksum) {
          bcrypt.hash(sha1(rows[i].email), config.bcrypt.rounds, function(err, hash) {
            db.query("UPDATE Users SET password=? WHERE user=?", [hash, rows[i].user], (err, result) => {
              if (err) {
                throw err;
              }
              else {
                console.log("User " + rows[i].user + " - password successfully reset");
              }
            });
          });
        }
      }
    }
  });
  res.redirect('/');
});

app.get('/password-reset', (req, res) => {
  res.render('password-reset.ejs', {message: ""});
});

app.post('/password-reset', (req, res) => {
  db.query("SELECT * FROM Users WHERE email=?", [req.body.email], (err, rows) => {
    if (err) {
      throw err;
    }
    else {
      if (rows.length === 0) {
        res.render('password-reset.ejs', {message: "Email does not exist"});
      }
      else {
        sendResetEmail(req.body.email);
        res.render('password-reset.ejs', {message: "Password reset email sent"});
      }
    }
  });
});

app.get('/profile', (req, res) => {
  if (req.session && req.session.user) {
    db.query("SELECT * FROM Users WHERE user=?", [req.session.user], (err, rows) => {
      if (err) {
        throw err;
      }
      res.render('profile.ejs', {
        id : rows[0].id,
        user: rows[0].user,
        email: rows[0].email,
        score: rows[0].score,
        message: ""
      });
    });
  }
  else {
    res.redirect('/');
  }
});

app.post('/profile-update', (req, res) => {
  //Get old creds for user
  db.query("SELECT * FROM Users WHERE user=?", [req.session.user], (err, rows) => {
    if (err) {
      throw err;
    }
    else {
      var oldUser = req.session.user;
      var oldEmail = rows[0].email;
      var oldPassword = rows[0].password;
      var oldID = rows[0].id;

      var newUser = req.body.newUser;
      var newEmail = req.body.newEmail;
      var newPassword = req.body.newPassword;
      var newCheckPassword = req.body.newConfPassword;

      if (req.body.newUser === "") {
        newUser = oldUser;
      }
      if (req.body.newEmail === "") {
        newEmail = oldEmail;
      }
      if (req.body.newPassword === "") {
        newPassword = oldPassword;
        newCheckPassword = oldPassword;
      }
    }
    validateUser(oldID, newUser, newEmail, newPassword, newCheckPassword).then(function(result) {
      if (result === true) {
        if (req.body.newPassword !== "") {
          bcrypt.hash(newPassword, config.bcrypt.rounds, function(err, hash) {
            db.query("UPDATE Users SET user=?, password=?, email=? WHERE id=?", [newUser, hash, newEmail, oldID], (err, rows) => {
              if (err) {
                throw err;
              }
              else {
                console.log("User: " + newUser + " credentials updated");
                req.session.user = newUser;
                res.redirect('/profile');
              }
            });
          });
        }
        else {
          db.query("UPDATE Users SET user=?, password=?, email=? WHERE id=?", [newUser, newPassword, newEmail, oldID], (err, rows) => {
            if (err) {
              throw err;
            }
            else {
              console.log("User: " + newUser + " credentials updated");
              req.session.user = newUser;
              res.redirect('/profile');
            }
          });
        }
      }
      else {
        console.log("New user credentials invalid");
        res.render('profile.ejs', {
          id : rows[0].id,
          user: rows[0].user,
          email: rows[0].email,
          score: rows[0].score,
          message: result
        });
      }
    });
  });
});

app.post('/delete-account', (req, res) => {
  authenticateLogin(req.session.user, req.body.password).then(function(result){
    if (result === true) {
      db.query("DELETE FROM Users WHERE user=?", [req.session.user], (err, res) => {
        if (err) {
          throw err;
        }
        else {
          console.log("User " + req.session.user + " deleted");
        }
      });
      res.redirect('/logout');
    }
    else {
      console.log("Deletion auth failed");
      res.redirect('/profile');
    }
  });
})

app.get('*', (req, res) => {
  res.redirect('/');
});

httpsServer.listen(config.site.port, function() {
  console.log("Listening on port %d", config.site.port);
});
