$(function () {
  var socket = io();
  var room;
  var userID;
  var screen_width = screen.width;

  $(document).ready(function() {
    if (screen_width < 500) {
      console.log("Screen width < 500");
      document.getElementById("scratchpad").style.setProperty("--column-1-width", "90vw");
      document.getElementById("scratchpad").style.setProperty("--column-1-width", "90vw");
      document.getElementById("scratchpad").style.setProperty("--column-2-width", "0");
    }
  });

  socket.on('start message', function(msg, id, subject){
    $('.chatbox').append('<h2>You are arguing about: ' + subject + '</h2>');
    $('#messages').append($('<li>').text(msg));
    userID = id;
    room = subject;
    console.log("My ID is " + userID);
    console.log("This room is " + room);
  });

  socket.on('join message', function(msg, id, subject){
    $('#messages').append($('<li>').text(msg));
  });

  $('form').submit(function(){
    socket.emit('chat message', $('#m').val(), userID);
    $('#m').val('');
    return false;
  });

  socket.on('chat message', function(msg){
    //console.log("Donny's test = " + msg + ".");
    $('#messages').append($('<li>').text(msg));
  })
});
