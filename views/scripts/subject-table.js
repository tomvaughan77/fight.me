$(function(){
  var socket = io();
  $(document).ready(function() {
    var $rows = $('#subject-table tbody tr');
    socket.on('table update', function(timestamp, totalUsers, table) {
      $('#total-users h3').remove();
      $('#total-users').append('<h3>In the mood for a good debate? Passionate about something with nobody to talk to? Or just feeling feisty? Join ' + totalUsers + ' other users and argue away.</h3>');
      $('#subject-table-updated p').remove();
      $('#subject-table-updated').append('<p>' + timestamp + '</p>');
      $('#subject-table tbody').remove();
      $('#subject-table').append('<tbody></tbody>');
      for (let i=0; i<table.length; i++){
        if (table[i].players > 0) {
          $('#subject-table > tbody').append('<tr></tr>');
          $('#subject-table > tbody > tr:last-child').append('<td>' + i + '</td>');
          $('#subject-table > tbody > tr:last-child').append('<td>' + table[i].name + '</td>');
          $('#subject-table > tbody > tr:last-child').append('<td>' + table[i].players + '</td>');
          if (session && session.user) {
            $('#subject-table > tbody > tr:last-child').append('<td><form action=\"join-chatroom\" method=\"POST\" name=\"chatroom-link\"><input type=\"hidden\" name=\"isNew\" value=\"false\"><input type=\"hidden\" name=\"subject\" value=\"' + table[i].name + '\"><input type=\"submit\" value=\"Join Chatroom\"></form></td>');
          }
        }
      }
      $rows = $('#subject-table tbody tr');
    });

    socket.on('suggested update', function(suggestedSubjects) {
      for (let i=0; i<suggestedSubjects.length; i++) {
        $('#suggested-table > tbody').append('<tr></tr>');
        $('#suggested-table > tbody > tr:last-child').append('<td>' + suggestedSubjects[i] + '</td>');
        if (session && session.user) {
          $('#suggested-table > tbody > tr:last-child').append('<td><form action=\"join-chatroom\" method=\"POST\" name=\"chatroom-link\"><input type=\"hidden\" name=\"isNew\" value=\"false\"><input type=\"hidden\" name=\"subject\" value=\"' + suggestedSubjects[i] + '\"><input type=\"submit\" value=\"Join Chatroom\"></form></td>');
        }
      }
    });
    $('#subject-table-search').keyup(function() {
      var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
      $rows.show().filter(function() {
          var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
          return !~text.indexOf(val);
      }).hide();
    });

    setInterval(function () {
    	pageScroll();
    }, (100));
  });

  // Change the selector if needed
  var $table = $('table.scroll'),
      $bodyCells = $table.find('tbody tr:first').children(),
      colWidth;

  var my_time;


  function pageScroll() {
  	var objDiv = document.getElementById("table_body");
    var r = document.getElementById("suggested-table").getElementsByTagName("tr").length;
    objDiv.scrollTop = objDiv.scrollTop + 1;
    if (objDiv.scrollTop == (objDiv.scrollHeight - r)) {
      objDiv.scrollTop = 0;
    }
   my_time = setTimeout('pageScroll()', 10);
  }

  // Adjust the width of thead cells when window resizes
  $(window).resize(function() {
      // Get the tbody columns width array
      colWidth = $bodyCells.map(function() {
          return $(this).width();
      }).get();

      // Set the width of thead columns
      $table.find('thead tr').children().each(function(i, v) {
          $(v).width(colWidth[i]);
      });
  }).resize(); // Trigger resize handler
});
